function submit_form() {
	var error = false;
	
	$('.form-input').each(function() {
		var id = $(this).attr('id');
		if ($('#'+id).hasClass('required') && $('#'+id).val() == '') {
			//var label = $('label[for="' + id + '"]').html();        
			$('#return-note').html('Please complete all required fields');
			error = true;
		}
		
		if (!error && id == '8c21b3e9467a78fa9ee13717370ebdde-2' && !IsEmail($('#'+id).val())) {
			//alert($('#'+id).val());
			$('#return-note').html('Please enter a valid email address');
			error = true;
		}


    })

	if (error == false) {
		var form_data = {
			firstname: $('#8c21b3e9467a78fa9ee13717370ebdde-0').val(),
			lastname: $('#8c21b3e9467a78fa9ee13717370ebdde-1').val(),
			email: $('#8c21b3e9467a78fa9ee13717370ebdde-2').val(),
			company: $('#8c21b3e9467a78fa9ee13717370ebdde-3').val(),
			role: $('#8c21b3e9467a78fa9ee13717370ebdde-4').val()
		};
				
		$.ajax({ 
			url: "http://cxpert.com.au/signup/script/mailchimp.php",
			data: form_data,
			type: "POST",
			beforeSend:function(){
				$('#submit_button').html('Sending...');
			},
			success: function(msg){
				if (msg) {
					$('#return-note').html('Sorry, there has been an error. Please contact ben@cxpert.com.au');
				}
				else {
					$('#submit_button').hide();
					$('#return-note').html('<span style="color:green">Thank you! You will receive an email with a link to download the  whitepaper. <a href="http://cxpert.com.au">Continue to cxpert.com.au</a></span>');
					$('#8c21b3e9467a78fa9ee13717370ebdde-0').val('');
					$('#8c21b3e9467a78fa9ee13717370ebdde-1').val('');
					$('#8c21b3e9467a78fa9ee13717370ebdde-2').val('');
					$('#8c21b3e9467a78fa9ee13717370ebdde-3').val('');
					$('#8c21b3e9467a78fa9ee13717370ebdde-4').val('');
				}
				$('#submit_button').html('DOWNLOAD NOW');
			}
		});
	}
	
	return false;
}

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}