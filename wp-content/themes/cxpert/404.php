<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


	<div class="container">
		<div class="row">
        	<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 col-pad inner-page">
                <h1>Page not found</h1>
                <p>The page you are looking for cannot be found.</p>
            </div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-pad blog-posts">
				<?php dynamic_sidebar( 'right_1' ); ?>
			</div>
		</div>
	</div><!--/.container -->

<?php
get_footer();
