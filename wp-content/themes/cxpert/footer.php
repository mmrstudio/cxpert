<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>


		<div class="footer">

			<div style="background: #1e1e1e;">
				<div class="container">
					<div class="row">
			            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 foot_col_1">
			            	<?php dynamic_sidebar( 'foot_col_1' ); ?>
			            </div><!--/span-->
			            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			            	<?php dynamic_sidebar( 'foot_col_2' ); ?>
			            </div><!--/span-->
			            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			            	<?php dynamic_sidebar( 'foot_col_3' ); ?>
			            </div><!--/span-->
					</div>
		      </div>
	      </div>

	    	<div class="container-fluid" style="background: #000000;" >
		      	<div class="navbar navbar-default" role="navigation">
					<div class="container">
						<div class="row">
				            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="navbar-header">
						        	<span class="navbar-brand">Copyright CXpert 2014 website by <a href="http://mmr.com.au/" target="_blank">MMR Studio</a></span>
								</div>
								<div class="navbar-collapse collapse">
							        <ul class="nav navbar-nav navbar-right">
										<li><a href="https://www.facebook.com/CXpertConsulting" target="_blank" title="CXpert on Facebook"><i class="fa fa-facebook"></i></a></li>
										<li><a href="https://twitter.com/CXpert" target="_blank"><i class="fa fa-twitter" title="CXpert on Twitter"></i></a></li>
										<li><a href="http://au.linkedin.com/pub/ben-motteram/0/935/364" target="_blank" title="CXpert on Linkedin"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	    </div>

	<?php wp_footer(); ?>
</body>
</html>