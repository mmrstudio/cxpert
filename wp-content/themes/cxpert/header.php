<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage cxpert
 * @since cxpert 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54696b38654d0a54"></script> 
    <script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-50134961-1', 'auto');
		ga('send', 'pageview');
	</script>
    
    <!-- Facebook Conversion Code for Key Page Views -->
	<script>(function() {
      var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
      }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6016205256922', {'value':'0.00','currency':'AUD'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6016205256922&amp;cd[value]=0.00&amp;cd[currency]=AUD&amp;noscript=1" /></noscript>
</head>

<body>

    
	<div class="container masthead">
		<div class="row" style="display: flex;">
        		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                	<a href="<?php echo home_url(); ?>">
			    		<img src="<?php echo get_template_directory_uri().'/images/CXpert-logo.jpg' ?>" class="img-responsive center-block">
                    </a>
			    </div>
                
			    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 text-center">
			    	<h1><?php echo get_bloginfo ( 'description' ); ?></h1>
                </div>
                 
                 <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                 	<div class="sticker">
                        <a href="<?php echo get_permalink(131); ?>">Introductory Offer</a>
                    </div>
                 </div>
	    </div>
	</div><!--/.container -->

		<div class="container-fluid" style="background: #000000; ">
			<div class="navbar navbar-default" role="navigation">

				<div class="container">

					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				
					<div class="navbar-collapse collapse">

						<?php wp_nav_menu(array(
							'theme_location'  => 'main-menu',
							'menu'            => '',
							'container'       => false,
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => 'nav navbar-nav',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 0,
							'walker'          => ''
						));?>
                        
                        <?php get_search_form( true ); ?>

						<ul class="nav navbar-nav navbar-right">
						  <li><a href="https://www.facebook.com/CXpertConsulting" target="blank" title="CXpert on Facebook"><i class="fa fa-facebook"></i></a></li>
						  <li><a href="https://twitter.com/CXpert" target="blank"><i class="fa fa-twitter" title="CXpert on Twitter"></i></a></li>
						  <li><a href="http://au.linkedin.com/pub/ben-motteram/0/935/364" target="blank" title="CXpert on Linkedin"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
	      </div>

