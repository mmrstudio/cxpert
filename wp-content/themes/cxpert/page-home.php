<?php
/*
Template Name: Home Page
*/

get_header(); ?>

	<div style="background:#123e6d;" class="container-fluid">
		<img src="<?php echo get_template_directory_uri().'/images/banner.jpg' ?>" class="img-responsive center-block">
	</div>
		
	</div>
<div class="container">
	<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 home-body-col">
        	<img src="<?php echo get_template_directory_uri().'/images/icon_02.jpg' ?>">
        	<?php dynamic_sidebar( 'home_col_1' ); ?>
        </div><!--/span-->
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 home-body-col">
        	<img src="<?php echo get_template_directory_uri().'/images/icon_04.jpg' ?>">
        	<?php dynamic_sidebar( 'home_col_2' ); ?> 
        </div><!--/span-->
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 home-body-col">
        	<img src="<?php echo get_template_directory_uri().'/images/icon_06.jpg' ?>">
        	<?php dynamic_sidebar( 'home_col_3' ); ?>
        </div><!--/span-->
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 home-body-col">
        	<img src="<?php echo get_template_directory_uri().'/images/icon_08.jpg' ?>">
			<?php dynamic_sidebar( 'home_col_4' ); ?>
        </div><!--/span-->
	</div>
</div><!--/.container -->

	<div class="container-fluid home-middle">
        <div class="container">
        	<div class="row ">
        		<div class="col-12 col-sm-12 col-lg-12">
        			<?php dynamic_sidebar( 'home_middle' ); ?>
        		</div>
        	</div>
		</div><!--/.container -->
	</div>
	
    <div class="container-fluid home-grey">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-6 col-pad">
                    <?php
                        if ( have_posts() ) :
                            // Start the Loop.
                            while ( have_posts() ) : the_post();
    
                                the_content();
    
                            endwhile;
    
                        else :
                            echo "no results fond!";
    
                        endif;
                    ?>
                </div>
    
                <div class="col-12 col-sm-12 col-lg-6 col-pad blog-posts">
                    <?php dynamic_sidebar( 'right_1' ); ?>
                    
                </div>
            </div>
        </div><!--/.container -->
    </div>

<?php

get_footer();
