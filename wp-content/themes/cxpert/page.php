<?php
/**
 * The main page template file
 *
 */

get_header(); ?>

	<div style="background:#123e6d url(<?php echo get_template_directory_uri().'/images/banner_inner.jpg' ?>) center; height:115px;" class="container-fluid"></div>

	<div class="container">
		<div class="row">
        	<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 col-pad inner-page">
				<?php
					if ( have_posts() ) :
						// Start the Loop.
						while ( have_posts() ) : the_post();
							echo "<h2>";
							the_title();
							echo "</h2>";

							the_content();

						endwhile;

					else :
						echo "no results fond!";

					endif;
				?>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-pad blog-posts">
				<?php dynamic_sidebar( 'right_1' ); ?>
			</div>
		</div>
	</div><!--/.container -->

<?php
get_footer();
