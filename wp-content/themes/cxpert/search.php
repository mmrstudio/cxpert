<?php
 get_header(); ?>

	<div style="background:#123e6d url(<?php echo get_template_directory_uri().'/images/banner_inner.jpg' ?>) center; height:115px;" class="container-fluid"></div>

	<div class="container">
		<div class="row">
        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-pad">
        		<?php
				$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

				$posts_array = new WP_Query( array(
					'posts_per_page'   => 10,
					'paged'			=> get_query_var("paged"),
					'orderby'          => 'post_date',
					'order'            => 'DESC',
					's'				=> get_query_var("s"),
					//'post_type'        => 'post',
					'post_status'      => 'publish',
					'suppress_filters' => true ) );
			
			if($posts_array->have_posts()) :
			?>
            <h2>Search results for &quot;<?php echo get_query_var("s"); ?>&quot;</h2>
			<ul>
			<?php
			while ( $posts_array->have_posts() ) : $posts_array->the_post(); ?>
				<li>
					<h4><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h4>
					<p><?php echo wp_trim_words(get_the_content(), 50, '<br><a class="more-link" href="'.get_permalink().'">....READ MORE</a>'); ?></p>
				</li>
			<?php 
			endwhile;
			?>
			</ul>
			<?php else : ?>
				<h2>no results fond for &quot;<?php echo get_query_var("s"); ?>&quot;!</h2>
			<?php endif;
			
			if(function_exists("sb_paginate")) sb_paginate(array("query" => $posts_array, "anchor" => 1, "range" => 1, "gap" => 3, "style" => "dark", "border_radius" => "none"));
			
			wp_reset_postdata();

			?>
			</div>
		</div>
	</div><!--/.container -->

<?php
get_footer();