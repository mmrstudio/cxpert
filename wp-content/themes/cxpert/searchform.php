
<form class="navbar-form navbar-left" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
	<div class="input-group">
        <input type="text" class="form-control input-sm" placeholder="Search"  value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>">
        <div class="input-group-btn">
            <button type="submit" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-search"></i></button>
        </div>
    </div>
</form>
