<?php
/**
 * The main single post template file
 *
 */

get_header(); ?>

	<div style="background:#123e6d url(<?php echo get_template_directory_uri().'/images/banner_inner.jpg' ?>) center; height:115px;" class="container-fluid"></div>

	<div class="container">
    	
		<div class="row">
        	<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 col-pad">
              
				<?php
					if ( have_posts() ) :
						// Start the Loop.
						while ( have_posts() ) : the_post();
							echo "<h2>";
							the_title();
							echo "</h2>";
				?>			
							<!-- Go to www.addthis.com/dashboard to customize your tools -->
							<div class="addthis_sharing_toolbox"></div>
                <?php            
							the_content();
				?>
                			
							<div class="addthis_sharing_toolbox"></div>
                <?php             
						endwhile;

					else :
						echo "no results fond!";

					endif;
				?>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-pad blog-posts">
            	
	
				<?php dynamic_sidebar( 'right_1' ); ?>
				<br>
                <!-- Begin MailChimp Signup Form -->
                    <link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                           We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
						 #mc_embed_signup h2 {
							  font-family: "din_alternatebold";
							 font-size: 21px;
							 font-weight:normal;
						} 
						#mc_embed_signup .mc-field-group input{
							padding:4px 0;
						}
						#mc_embed_signup .indicates-required {
							display:none;
						}
						#mc_embed_signup div#mce-responses {
							clear: both;
							float: left;
							margin: 0 0%;
							overflow: hidden;
							padding: 0 0em;
							top: -1.4em;
							width: 100%;
						}
						#mc_embed_signup div.response {
							
							margin: 1em 0;
							padding: 0;
							top: -1.5em;
							width: 100%;
						}
						 
                    </style>
                    <div id="mc_embed_signup">
                    <form action="//cxpert.us9.list-manage.com/subscribe/post?u=21199ddf063b663fb018ffcc6&amp;id=4fe5331026" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                        <h2>Subscribe to Cxpert</h2>
                    <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                    <div class="mc-field-group">
                        <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
                    </label>
                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                    </div>
                    <div class="mc-field-group">
                        <label for="mce-FNAME">Full Name  <span class="asterisk">*</span>
                    </label>
                        <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
                    </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;"><input type="text" name="b_21199ddf063b663fb018ffcc6_4fe5331026" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                        </div>
                    </form>
                    </div>
                    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
					<!--End mc_embed_signup-->
                
           </div>
         </div> <!--row-->
     </div><!--/.container -->      
                
   <div class="container-fluid home-middle">
        <div class="container">
            <div class="row ">
                <div class="col-12 col-sm-12 col-lg-12">
                </div>
            </div>
        </div><!--/.container -->
    </div>
    <div class="container-fluid home-grey">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-pad">
                	<?php comments_template( '', true ); ?>
                </div>
            </div>
        </div><!--/.container -->
    </div>     